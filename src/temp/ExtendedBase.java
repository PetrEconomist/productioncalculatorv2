package temp;

public class ExtendedBase extends Base{
		
	@Override
	public void more() {
		super.value *= 2;
	}
	
	public int getValue() {
		return super.value;
	}
	
	public static void main(String[] args) {
		ExtendedBase base = new ExtendedBase();
		System.out.println(base.getValue());
		base.more();
		System.out.println(base.getValue());
		base.more();
		System.out.println(base.getValue());
		base.more();
		System.out.println(base.getValue());
		base.more();
		System.out.println(base.getValue());
		base.less();
		System.out.println(base.getValue());
		base.less();
		System.out.println(base.getValue());
		base.less();
		System.out.println(base.getValue());
	}
}
