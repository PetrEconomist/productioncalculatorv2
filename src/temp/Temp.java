package temp;

import static java.util.Map.entry;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Temp {
	public static void main(String[] args) {
		Foo f = new Foo(1);
		System.out.println("prior " + f);
		System.out.println("increment " + increment(f));
		System.out.println("after " + f);
		sumMap();

	}

	private static Foo increment(Foo f) {
		Foo g = f;
		g.increment();
		return g;
	}

	private static class Foo {
		int x;

		Foo(int x) {
			this.x = x;
		}

		void increment() {
			x++;
		}

		public String toString() {
			return "Foo value is " + x;
		}
	}

	private static void processMap(Map<String, Double> map, Consumer<Double> c) {
		for (Double d : map.values()) {
			c.accept(d);
		}
	}

	private static void sumMap() {
		Map<String, Double> map = Map.ofEntries(entry("a", 1.0), entry("b", 2.0));
		processMap(map, val -> System.out.println(val));
		System.out.println(map.values().stream().mapToDouble(Double::doubleValue).sum());
		System.out.println("printing keys using stream");
		map.keySet().stream().forEach(value -> printout(value));

	}

	private static void printout(String s) {
		System.out.println(s);
	}
}
