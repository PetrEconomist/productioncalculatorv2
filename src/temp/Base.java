package temp;

public class Base {
	int value = 1;
	
	public void more() {
		value++;
	}
	
	public void less() {
		value--;
	}
	
	public int getValue() {
		return value;
	}
}
