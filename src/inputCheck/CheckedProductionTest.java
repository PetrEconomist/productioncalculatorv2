package inputCheck;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CheckedProductionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckedProduction() {
		String input = "FAxyz;B;A;6;2";
		CheckedProduction cp = new CheckedProduction(input);
		assertEquals(6.0, cp.getConsumedUnits(), 0.0);
		assertEquals(2.0, cp.getProducedUnits(), 0.0);
		assertEquals(3.0, cp.getCheckedValue(), 0.0);
		assertEquals("FAxyz", cp.getRecordID());
		assertEquals("B;A", cp.getCheckedID());
	}

}
