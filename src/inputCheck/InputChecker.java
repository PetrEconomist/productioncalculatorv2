package inputCheck;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Class checks data for deviations.
 * @author pope
 *
 */
public class InputChecker {
	/**
	 * Limit of absolute relative deviation.
	 */
	private static double SIGNIFICANT_DEVIATION = 0.05;

	/**
	 * Stores data for test. Key is ID to be checked.
	 */
	private Map<String, LinkedList<CheckedEntity>> testedData = new HashMap<String, LinkedList<CheckedEntity>>();

	/**
	 * 
	 * @param input input must be in following structure: recordID;checkedID;checkedValue<br>
	 *              CheckedID can contain ';', recordID and checkedVal must NOT
	 *              contain ';'
	 */
	public void importData(LinkedList<String> input) {
		for (String inputLine : input) {
			CheckedEntity ce = new CheckedEntity(inputLine);
			importCheckedEntity(ce);
		}
	}
	
	
	/**
	 * 
	 * @param input input must be in following structure: recordID;checkedID;checkedValue<br>
	 *              CheckedID can contain ';', recordID and checkedVal must NOT
	 *              contain ';'
	 */
	public InputChecker(LinkedList<String> input){
		importData(input);
	}

	void importCheckedEntity(CheckedEntity ce) {
		String ceID = ce.getCheckedID();
		if (!testedData.containsKey(ceID)) {
			testedData.put(ceID, new LinkedList<CheckedEntity>());
		}
		testedData.get(ceID).add(ce);
	}

	/**
	 * Finds deviated items in the data.
	 * @return values that vary significantly from usual value.
	 */
	public LinkedList<CheckedEntity> getDeviatedItems() {
		LinkedList<CheckedEntity> deviatedItems = new LinkedList<CheckedEntity>();
		for (String combination : testedData.keySet()) {
			LinkedList<CheckedEntity> records = testedData.get(combination);
			double meanVal = getMadianValue(records);
			System.out.println("Analysis of " + combination + ", mean value " + meanVal);
			for (CheckedEntity record : records) {
				if (Math.abs(1 - (record.getCheckedValue() / meanVal)) > SIGNIFICANT_DEVIATION) {
					deviatedItems.add(record);
				}
			}
		}
		return deviatedItems;
	}

	private double getMadianValue(LinkedList<CheckedEntity> records) {
		LinkedList<Double> values = new LinkedList<Double>();
		for (CheckedEntity ce : records) {
			values.add(ce.getCheckedValue());
		}
		return getMedian(values.toArray(new Double[values.size()]));
	}
	
	private double getMedian(Double[] numArray) {
		Arrays.sort(numArray);
		if (numArray.length % 2 == 0)
			return ((double) numArray[numArray.length / 2] + (double) numArray[numArray.length / 2 - 1]) / 2;
		else
			return (double) numArray[numArray.length / 2];
	}

}
