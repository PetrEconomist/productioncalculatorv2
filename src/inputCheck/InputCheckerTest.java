package inputCheck;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class InputCheckerTest {
	
	LinkedList<String> testInput;
	String invalidIn2 = "in2;B;A;8.0";
	String invalidIn8 = "in8;B;A;2.0";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testInput = new LinkedList<String>();
		testInput.add("in0;B;A;5");
		testInput.add("in1;B;A;5.1");
		testInput.add(invalidIn2);
		testInput.add("in3;B;A;4.9");
		testInput.add("in4;B;A;5.1");
		testInput.add("in5;B;A;4.9");
		testInput.add("in6;C;A;4.9");
		testInput.add("in7;X;Z;4.9");
		//in8 separately
		testInput.add("in9;A;Z;4.9");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetDeviatedItemsEvenInput() {
		LinkedList<CheckedEntity> expected = new LinkedList<CheckedEntity>();
		expected.add(new CheckedEntity(invalidIn2));
		InputChecker ic = new InputChecker(testInput);
		assertEquals(expected, ic.getDeviatedItems());
	}

	@Test
	public void testGetDeviatedItemsOddInput() {
		testInput.add(invalidIn8);
		LinkedList<CheckedEntity> expected = new LinkedList<CheckedEntity>();
		expected.add(new CheckedEntity(invalidIn2));
		expected.add(new CheckedEntity(invalidIn8));
		InputChecker ic = new InputChecker(testInput);
		assertEquals(expected, ic.getDeviatedItems());
	}

}
