package inputCheck;

import java.util.LinkedList;



public class ProductionChecker extends InputChecker {

	public ProductionChecker(LinkedList<String> input) {
		super(input);
	}

	@Override
	public void importData(LinkedList<String> input) {
		for (String inputLine : input) {
			CheckedProduction cp = new CheckedProduction(inputLine);
			super.importCheckedEntity((CheckedEntity) cp);
		}
	}
	

	
}
