package inputCheck;

public class CheckedProduction extends CheckedEntity {

	private final double producedUnits;
	private final double consumedUnits;

	private static InputCrate convertInput(String input) {
		return new InputCrate(input);
	}

	CheckedProduction(String input) {
		super(convertInput(input).getCheckedEntity());
		InputCrate ic = convertInput(input);
		producedUnits = ic.getProducedUnits();
		consumedUnits = ic.getConsumedUnits();
	}

	public double getProducedUnits() {
		return producedUnits;
	}

	public double getConsumedUnits() {
		return consumedUnits;
	}

	private static class InputCrate {
		private final double producedUnits;
		private final double consumedUnits;
		private final CheckedEntity ce;

		InputCrate(String input) {
			String inputConverted = input;
			int lastDelimPos = inputConverted.lastIndexOf(DELIMITER);
			String numberDelim = "/";
			inputConverted = inputConverted.substring(0, lastDelimPos) + numberDelim
					+ inputConverted.substring(lastDelimPos + 1);
			lastDelimPos = inputConverted.lastIndexOf(DELIMITER);
			int numberDelimPos = inputConverted.lastIndexOf(numberDelim);
			consumedUnits = Double.parseDouble(inputConverted.substring(lastDelimPos + 1, numberDelimPos));
			producedUnits = Double.parseDouble(inputConverted.substring(numberDelimPos + 1));
			double checkedVal = consumedUnits / producedUnits;
			inputConverted = inputConverted.substring(0, lastDelimPos) + DELIMITER + checkedVal;
			ce = new CheckedEntity(inputConverted);
		}

		CheckedEntity getCheckedEntity() {
			return ce;
		}

		public double getProducedUnits() {
			return producedUnits;
		}

		public double getConsumedUnits() {
			return consumedUnits;
		}
	}
}
