package inputCheck;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ProductionCheckerTest {

	LinkedList<String> testInput;
	String invalidIn2 = "in2;B;A;24.0;3";
	String invalidIn8 = "in8;B;A;2.0;1";
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testInput = new LinkedList<String>();
		testInput.add("in0;B;A;10;2");
		testInput.add("in1;B;A;5.1;1");
		testInput.add(invalidIn2);
		testInput.add("in3;B;A;4.9;1");
		testInput.add("in4;B;A;10.2;2");
		testInput.add("in5;B;A;14.7;3");
		testInput.add("in6;C;A;4.9;1");
		testInput.add("in7;X;Z;4.9;1");
		//in8 separately
		testInput.add("in9;A;Z;4.9;1");
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testGetDeviatedItemsEvenInput() {
		LinkedList<CheckedProduction> expected = new LinkedList<CheckedProduction>();
		expected.add(new CheckedProduction(invalidIn2));
		ProductionChecker pc = new ProductionChecker(testInput);
		assertEquals(expected, pc.getDeviatedItems());
	}

	@Test
	public void testGetDeviatedItemsOddInput() {
		testInput.add(invalidIn8);
		LinkedList<CheckedProduction> expected = new LinkedList<CheckedProduction>();
		expected.add(new CheckedProduction(invalidIn2));
		expected.add(new CheckedProduction(invalidIn8));
		ProductionChecker pc = new ProductionChecker(testInput);
		assertEquals(expected, pc.getDeviatedItems());
	}

}
