package inputCheck;

import java.util.Objects;

/**
 * Class stores data to be analyzed.
 * @author pope
 *
 */
class CheckedEntity {
	/**
	 * Identifier of the record. Should be unique or with limited repetition
	 */
	private final String recordID;
	/**
	 * Identifier of case to be analyzed
	 */
	private final String checkedID;
	/**
	 * Value to be analyzed
	 */
	private final double checkedValue;

	/**
	 * Delimiter of input data
	 */
	static final String DELIMITER = ";";

	CheckedEntity(String recordID, String checkedID, double checkedVal) {
		this.recordID = recordID;
		this.checkedID = checkedID;
		this.checkedValue = checkedVal;
	}
	
	CheckedEntity(CheckedEntity ce){
		this.recordID = ce.getRecordID();
		this.checkedID = ce.getCheckedID();
		this.checkedValue = ce.getCheckedValue();
	}

	/**
	 * 
	 * @param input input must be in following structure: recordID;checkedID;checkedValue<br>
	 *              CheckedID can contain ';', recordID and checkedVal must NOT
	 *              contain ';'
	 */
	CheckedEntity(String input) {
		int firstDelimPos = input.indexOf(DELIMITER);
		int lastDelimPos = input.lastIndexOf(DELIMITER);
		recordID =  input.substring(0, firstDelimPos);	
		checkedID = input.substring(firstDelimPos + DELIMITER.length(), lastDelimPos);
		checkedValue = Double.parseDouble(input.substring(lastDelimPos + DELIMITER.length()));		
	}

	/**
	@Override
	public String toString() {
		return "CheckedEntity [checkedID=" + checkedID + ", checkedValue=" + checkedValue + ", itemID=" + recordID + "]";
	}
	*/

	@Override
	public int hashCode() {
		return Objects.hash(checkedID, checkedValue, recordID);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CheckedEntity other = (CheckedEntity) obj;
		return Objects.equals(checkedID, other.checkedID)
				&& Double.doubleToLongBits(checkedValue) == Double.doubleToLongBits(other.checkedValue)
				&& Objects.equals(recordID, other.recordID);
	}

	public double getCheckedValue() {
		return checkedValue;
	}

	public String getRecordID() {
		return recordID;
	}

	public String getCheckedID() {
		return checkedID;
	}
	

}