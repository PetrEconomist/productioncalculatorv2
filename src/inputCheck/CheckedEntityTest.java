package inputCheck;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class CheckedEntityTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCheckedEntityStringStringDouble() {
		String itemID = "FAxyz";
		String checkedID = "B;A";
		double checkedVal = 0.5;
		CheckedEntity ce = new CheckedEntity(itemID, checkedID, checkedVal);
		assertEquals(itemID, ce.getRecordID());
		assertEquals(checkedID, ce.getCheckedID());
		assertEquals(checkedVal, ce.getCheckedValue(), 0.0);		
	}

	@Test
	public void testCheckedEntityString() {
		String itemID = "FAxyz";
		String checkedID = "B;A";
		double checkedVal = 0.5;
		String input = itemID + ";" + checkedID + ";" + checkedVal;
		CheckedEntity ce = new CheckedEntity(input);
		CheckedEntity expected = new CheckedEntity(itemID, checkedID, checkedVal);
		assertEquals(expected, ce);
	}
	
	@Test
	public void testGetHashCode() {
		String itemID = "FAxyz";
		String checkedID = "B;A";
		double checkedVal = 0.5;
		CheckedEntity ce = new CheckedEntity(itemID, checkedID, checkedVal);
		assertEquals(-1009249147, ce.hashCode());
	}
	
	@Test
	public void testEquals() {
		String itemID = "FAxyz";
		String checkedID = "B;A";
		double checkedVal = 0.5;
		CheckedEntity ce0 = new CheckedEntity(itemID, checkedID, checkedVal);
		CheckedEntity ce1 = new CheckedEntity(itemID, checkedID, checkedVal);
		CheckedEntity ce2 = new CheckedEntity(itemID, checkedID, checkedVal + 0.00001);
		CheckedEntity ce3 = new CheckedEntity(itemID + " ", checkedID, checkedVal);
		CheckedEntity ce4 = new CheckedEntity(itemID, checkedID + " ", checkedVal);
		CheckedEntity ce5 = new CheckedEntity(ce0);
		
		assertFalse(ce0.equals(null));
		assertFalse(ce0.equals(new Object()));
		assertTrue(ce0.equals(ce0));
		assertTrue(ce0.equals(ce1));
		assertFalse(ce0.equals(ce2));
		assertFalse(ce0.equals(ce3));
		assertFalse(ce0.equals(ce4));
		assertTrue(ce0.equals(ce5));	
	}

}
