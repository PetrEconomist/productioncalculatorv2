package userInterface;

import java.util.LinkedList;

import arrayFunctions.ArrayFunctionsGeneral;
import fileManipulation.ReadFile;
import fileManipulation.WriteFile;
import productionGraph.ProductionGraph;

public class Terminal {

	static private ProductionGraph g = new ProductionGraph();

	static void setGraph(ProductionGraph graph) {
		g = graph;
	}

	public static void main(String[] args) {
		while (true) {
			System.out.println("Main menu\n");
			ArrayFunctionsGeneral.printlnArray(new String[] { "Exit programm",
					"Calculate diluted graph per produced piece", "Print graph", "Filemanagement", });

			String choice = "";
			try {
				choice = KeyboardListener.listen();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			if (choice.equals("0")) {
				System.out.println("Exiting\n");
				break;
			} else if (choice.equals("1")) {
				calculateDilutedGraphPerPiece();
			} else if (choice.equals("2")) {
				//TODO printGraph();
			} else if (choice.equals("3")) {
				getDialogFilemanagement();
			} else {
				System.out.println("Invalid input");
			}
		}
	}

	private static void calculateDilutedGraphPerPiece() {
		ProductionGraph dilutedGraphPP = g.getDilutedConsumptionPerPiece();
		g = dilutedGraphPP;
		System.out.println("Graph diluted.");
	}

	private static void getDialogFilemanagement() {
		System.out.println("Filemanagement:");
		ArrayFunctionsGeneral.printlnArray(new String[] { "Import file", "Export file" });

		String choice = "";
		try {
			choice = KeyboardListener.listen();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		switch (choice) {
		case "0":
			getDialogInputfile();
			break;
		case "1":
			getDialogExportfile();
			break;
		default:
			System.out.println("Invalid");
			break;
		}
	}

	private static void getDialogInputfile() {
		System.out.println("Please enter filename for import");
		System.out.println(
				"Data structure: '(optional) record ID; consumedID; producedID; [consumed quantity; produced quantity]'");
		String fileName = "";
		try {
			fileName = KeyboardListener.listen();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LinkedList<String> input = ReadFile.getFileAsList(fileName);
		g.addEdgesFromInput(input);

	}

	static ProductionGraph getGraph() {
		return g;
	}

	/**
	static void printGraph() {
		System.out.println("Uploaded graph: " + g);
	}
	*/

	static void clearGraph() {
		g = new ProductionGraph();
	}

	private static void getDialogExportfile() {
		System.out.println("Please enter filename for export");
		String fileName = "";
		try {
			fileName = KeyboardListener.listen();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		LinkedList<String> exportBatch = new LinkedList<String>();
		exportBatch.add("consumed;produced;consumedQt;producedQt");
		exportBatch.addAll(g.getExportBatch());
		WriteFile.writeIntoFile(fileName, exportBatch.toArray(new String[exportBatch.size()]));
	}
	
	


}
