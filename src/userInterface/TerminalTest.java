package userInterface;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fileManipulation.ReadFile;
import productionGraph.ProductionGraph;

public class TerminalTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
		Terminal.clearGraph();
	}

	
	@Test
	public void testFileInput() throws Throwable {
		String importFile = "testInput.txt";
		System.out.printf("Test: import file '%s'\n", importFile);
		Terminal.main(null);
		ProductionGraph expected = new ProductionGraph();
		expected.addEdgesFromInput(ReadFile.getFileAsList(importFile));
		assertEquals(expected, Terminal.getGraph());
	}
	

	@Test
	public void testFileExport() throws Throwable {
		String fileName = "testOutput.txt";
		System.out.println("Test: export into file '" + fileName + "'");

		ProductionGraph graph = new ProductionGraph();

		LinkedList<String> testInput= new LinkedList<String>();
		testInput.add("C;A;[2.0]");
		testInput.add("B;A;[1.0]");
		testInput.add("E;C;[1.5]");
		testInput.add("D;C;[3.0]");

		graph.addEdgesFromInput(testInput);

		Terminal.setGraph(graph);
		Terminal.main(null);

		LinkedList<String> export = ReadFile.getFileAsList(fileName);
		
		LinkedList<String> testExpected = new LinkedList<String>();
		testExpected.add("consumed;produced;consumedQt;producedQt");
		testExpected.addAll(testInput);

		assertEquals(testExpected, export);
	}
	
	@Test
	public void testComplex() throws Throwable {
		String fileOut = "testOutput2.txt";
		System.out.println("Test: import graph from testInput2.txt, dilute and export into " + fileOut);
				
		Terminal.main(null);

		LinkedList<String> testExpected = new LinkedList<String>();
		testExpected.add("consumed;produced;consumedQt;producedQt");
		
		testExpected.add("B;A;[9.0, 1.0]");
		testExpected.add("D;A;[4.0, 1.0]");
		
		assertEquals(testExpected, ReadFile.getFileAsList(fileOut));
	}

	
	@Test
	public void testFileInvalidInput() throws Throwable {
		System.out.println("Test: make invalid choice in main menu'");
		Terminal.main(null);
		assertTrue(KeyboardListener.getYN("Works as expected?"));
	}
	

}
