package fileManipulation;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import userInterface.KeyboardListener;

public class WriteFile {

	private WriteFile() {
		throw new UnsupportedOperationException();
	};

	/**
	 * Appends existing file, each array field on separate line.
	 * 
	 * @param fileName   name of file to be written in
	 * @param inputLines lines to be loaded int the file
	 */

	private static void appendFile(File file, String[] inputLines) {
		int status = 0;
		for (int inLine = 0; inLine < inputLines.length; inLine++) {
			appendFile(file, inputLines[inLine] + "\n");
			int statusNew = (int) (100 * (double) inLine / inputLines.length);
			if (status != statusNew) {
				System.out.printf("exported %d%% lines into the file\n", statusNew);
				status = statusNew;
			}
		}
		System.out.println("File exported successfully\n");
	}

	/**
	 * Appends one line to the file
	 * 
	 * @param file file to be appended
	 * @param line line to be appended to the file
	 */
	private static void appendFile(File file, String line) {
		try {
			Files.write(Paths.get(file.getPath()), line.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}
	}

	public static File writeIntoFile(String fileName, String[] inputLines) {
		try {
			return writeIntoFile(new File(fileName), inputLines);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private static File writeIntoFile(File file, String[] inputLines) throws IOException {
		File fileOut = file;
		if (!fileOut.exists()) {
			writeIntoNewFile(fileOut, inputLines);
		} else {
			String newFileName = getFileNameWithTime(fileOut);
			System.out.printf("\nFile %s exists, please make selection:\n" + "0: Write into new file: %s\n"
					+ "1: append existing file\n" + "2: Overwrite existing file\n" + "3: Cancel export into file\n",
					fileOut.toString(), newFileName);
			String userIn = "";
			try {
				userIn = KeyboardListener.listen();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			switch (userIn) {
			case ("0"): { // write into new file
				fileOut = new File(newFileName);
				writeIntoNewFile(fileOut, inputLines);
				break;
			}
			case ("1"): { // append
				appendFile(fileOut, inputLines);
				break;
			}
			case ("2"): { // overwrite
				overwriteFile(fileOut, inputLines);
				break;
			}
			
			default: { // cancel
				fileOut = null;
				break;
			}
			}
		}
		return fileOut;
	}

	private static String getFileNameWithTime(File file) {
		DateFormat dateFormat = new SimpleDateFormat("yyyymmdd hhmmss");
		String strDate = dateFormat.format(new Date());
		String fileNameOut = file.getName();
		int pos = fileNameOut.lastIndexOf(".");
		int posDot = fileNameOut.lastIndexOf(".");
		if (posDot > 0) { // If '.' is not the first or last character.
			return fileNameOut = fileNameOut.substring(0, pos) + " " + strDate + fileNameOut.substring(pos);
		}
		return file.getName() + " " + strDate;
	}

	private static void overwriteFile(File file, String[] inputLines) throws IOException {
		file.delete();
		writeIntoNewFile(file, inputLines);
	}

	private static void writeIntoNewFile(File file, String[] inputLines) throws IOException {
		file.createNewFile();
		appendFile(file, inputLines);

	}

}
