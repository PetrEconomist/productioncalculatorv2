package graph;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import graph.edge.Edge;
import graph.node.Node;

public class GraphFunctionsTest extends GraphFunctions {

	@Test
	public void testDijkstra() {
		Graph g = new Graph();
		g.addEdge(new Edge("A", "B", new double[] {1.0}, "eAB"));
		g.addEdge(new Edge("A", "I", new double[] {40}, "eAI"));
		g.addEdge(new Edge("B", "D", new double[] {8.0}, "eBD"));
		g.addEdge(new Edge("B", "C", new double[] {2.0}, "eBC"));
		g.addEdge(new Edge("C", "D", new double[] {3.0}, "eCD"));
		g.addEdge(new Edge("D", "E", new double[] {4.0}, "eDE"));
		g.addEdge(new Edge("D", "F", new double[] {4.6}, "eDF"));
		g.addEdge(new Edge("E", "F", new double[] {0.5}, "eEF"));
		g.addEdge(new Edge("E", "J", new double[] {0.1}, "eEJ"));
		g.addEdge(new Edge("J", "K", new double[] {0.1}, "eJK"));
		g.addEdge(new Edge("K", "L", new double[] {0.1}, "eKL"));
		g.addEdge(new Edge("L", "I", new double[] {5}, "eLI"));
		g.addEdge(new Edge("F", "G", new double[] {0.3}, "eFG"));
		g.addEdge(new Edge("G", "H", new double[] {0.2}, "eGH"));
		g.addEdge(new Edge("H", "I", new double[] {1.0}, "eHI"));
		
		assertEquals((GraphFunctions.Dijkstra(g, 0, new Node("A"), new Node("I")) + ""), 
				"[Node [name=I], Node [name=H], Node [name=G], Node [name=F], Node [name=E], Node [name=D], Node [name=C], Node [name=B], Node [name=A]]");
		
		assertEquals((GraphFunctions.Dijkstra(g, 0, new Node("A"), new Node("E")) + ""), 
				"[Node [name=E], Node [name=D], Node [name=C], Node [name=B], Node [name=A]]");
	}

}
