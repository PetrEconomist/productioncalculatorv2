package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import graph.edge.Edge;
import graph.node.Node;

/**
 * Finds shortest path in the graph, where edges have values
 * 
 * @author Mamut
 *
 */
public class GraphFunctions {

	/**
	 * Vstup: Graf G a počáteční vrchol v0 
	 * 
	 * @param g graph in which we find shortest path
	 * @param valuePos position on which is in edge stored the distance between nodes
	 * @param valuePos startNode starting node of the path to the exit node
	 * @param valuePos exitNode exit node of the path from start node
	 * 
	 * 
	 * @return shortest path from start node to exit node
	 */
	public static LinkedList<Node> Dijkstra(Graph g,  int valuePos, Node startNode, Node exitNode) {
		HashMap<Node, Double> clocks = new HashMap<Node, Double>();
		HashMap<Node, Node> predecessors = new HashMap<Node, Node>();
		HashMap<Node, HashSet<Node>> successors = g.getSuccessors();
		HashSet<Node> nodes = g.getNodes();
		HashSet<Node> openedNodes = new HashSet<Node>();
		double counter = 0;
		
// 		Pro všechny vrcholy v:
//		stav(v) ← nenalezený
//		h(v) ← +∞
//		P(v) ← nedefinováno
//		
		for (Node node : nodes) {
			clocks.put(node, Double.MAX_VALUE);
			predecessors.put(node, null);
		}
//		stav(v0) ← otevřený
		openedNodes.add(startNode);
//		h(v0) ← 0
		clocks.put(startNode, 0.0);

//		Dokud existují nějaké otevřené vrcholy:
		while (!openedNodes.isEmpty()) {
//			Vybereme otevřený vrchol v, jehož h(v) je nejmenší.
			Node nodeWithMinClock = null;
			Double minClock = Double.MAX_VALUE;
			for (Node n : openedNodes) {
				if (clocks.get(n) < minClock) {
					nodeWithMinClock = n;
					minClock = clocks.get(n);
				}
			}
			HashSet<Node> successorsJustDiscovered = successors.get(nodeWithMinClock);
			if (successorsJustDiscovered != null) {
				// Pro všechny následníky w vrcholu v:
				for (Node successor : successorsJustDiscovered) {
					// Pokud h(w) > h(v) + vzdalenost(v, w):
					double distance = getMinDistance(g, valuePos, nodeWithMinClock, successor);
					if (clocks.get(successor) > minClock + distance) {
						// h(w) ← h(v) + vzdalenost(v, w)
						clocks.put(successor, minClock + distance);
						// stav(w) ← otevřený
						openedNodes.add(successor);
						// P(w) ← v
						predecessors.put(successor, nodeWithMinClock);
					}
				}
			}
//			stav(v) ← uzavřený
			openedNodes.remove(nodeWithMinClock);
			System.out.println("processed node " + counter++ + "/" + nodes.size());
		}

		LinkedList<Node> out = new LinkedList<Node>();
		out.add(exitNode);
		while(!out.getLast().equals(startNode)) {
			out.add(predecessors.get(out.getLast()));
		}
		return out;
	}

	public static double getMinDistance(Graph g, int valuePos, Node fromNode, Node toNode) {
		double min = Double.MAX_VALUE;
		HashSet<Edge> edges = g.getEdges(fromNode, toNode);
		for (Edge e : edges) {
			if (e.getValue()[valuePos] < min) {
				min = e.getValue()[valuePos];
			}
		}
		return min;
	}
	
}
