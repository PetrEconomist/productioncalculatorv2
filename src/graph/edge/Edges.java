package graph.edge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;

import graph.node.Node;

public class Edges {

	private EdgesNew edgesStorage;

	public Edges() {
		edgesStorage = new EdgesNew();
	}

	public Edges(Edges edges) {
		this.edgesStorage = new EdgesNew(edges.edgesStorage);
	}

	public void addEdge(Edge edge) {
		edgesStorage.addEdge(edge);
	}

	@Override
	public String toString() {
		return "Edges [edgesToNode=" + edgesStorage + "]";
	}

	/**
	 * @Override public int hashCode() { return Objects.hash(edgesStorage); }
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edges other = (Edges) obj;
		return Objects.equals(edgesStorage, other.edgesStorage);
	}

	public Set<Node> getLeaves() {
		Set<Node> leaves = new HashSet<Node>();
		Set<Node> notLeaves = edgesStorage.getParents();
		Set<Node> possibleLeaves = edgesStorage.getLeaves();
		for (Node n : possibleLeaves) {
			if (!notLeaves.contains(n))
				leaves.add(n);
		}
		return leaves;
	}

	public HashSet<Edge> getEdgesToNode(Node nodeTo) {
		return edgesStorage.getEdgesTo(nodeTo);
	}

	public void sumEdges() {
		edgesStorage.sumEdges();
	}

	/**
	 * public void averageEdges() { edgesStorage.averageEdges(); }
	 */

	public void calculateEdgesValue(ValueOperationCrate valueOperation) {
		edgesStorage.calculateEdgesValue(valueOperation);
	}

	public HashSet<Edge> getEdges() {
		return edgesStorage.getEdges();
	}

	public HashSet<Node> getSuccessors(Node n) {
		return getSuccessors().get(n);
	}

	public HashMap<Node, HashSet<Node>> getSuccessors() {
		return edgesStorage.getSuccessors();
	}

	public HashSet<Node> getPredecessors(Node n) {
		return getPredecessors().get(n);
	}

	public HashMap<Node, HashSet<Node>> getPredecessors() {
		return edgesStorage.getPredecessors();
	}

	public HashSet<Edge> getEdges(Node fromNode, Node toNode) {
		HashSet<Edge> edgesToNode = edgesStorage.getEdges(fromNode, toNode);
		HashSet<Edge> out = new HashSet<Edge>();
		for (Edge e : edgesToNode) {
			if (e.getNodeFrom().equals(fromNode)) {
				out.add(e);
			}
		}
		return out;
	}

	public LinkedList<String> getExportBatch() {
		return edgesStorage.getExportBatch();
	}

}
