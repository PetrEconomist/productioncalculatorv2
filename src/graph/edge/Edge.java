package graph.edge;

import java.util.Arrays;
import java.util.Objects;

import arrayFunctions.ArrayFunctionsGeneral;
import graph.node.Node;

public class Edge {

	private double[] values;
	private String edgeNotes;
	private Node fromNode, toNode;

	public Edge(Node fromNode, Node toNode, double[] values) {
		this(fromNode, toNode, values, "");
	}

	public Edge(Node fromNode, Node toNode, double[] values, String notes) {
		this.fromNode = fromNode;
		this.toNode = toNode;
		this.values = new double[values.length];
		for (int i = 0; i < values.length; i++) {
			this.values[i] = values[i];
		}
		this.edgeNotes = notes;
	}

	/**
	public Edge(Edge e) {
		this(e.getNodeFrom(), e.getNodeTo(), ArrayFunctionsGeneral.getCopy(e.getValue()), "");
	}
	*/

	public Edge(Node fromNode, Node toNode, double value) {
		this(fromNode, toNode, new double[] { value });
	}

	public Edge(Node fromNode, Node toNode, double value, String name) {
		this(fromNode, toNode, new double[] { value }, name);
	}

	public Edge(String fromNodeName, String toNodeName, double value) {
		this(new Node(fromNodeName), new Node(toNodeName), new double[] { value });
	}

	public Edge(String fromNodeName, String toNodeName, double[] values) {
		this(new Node(fromNodeName), new Node(toNodeName), values, "");
	}

	public Edge(String fromNodeName, String toNodeName, double[] values, String name) {
		this(new Node(fromNodeName), new Node(toNodeName), values, name);
	}

	public Edge(String fromNodeName, String toNodeName, double value, String name) {
		this(new Node(fromNodeName), new Node(toNodeName), new double[] { value }, name);
	}

	public Node getNodeFrom() {
		return fromNode;
	}

	public Node getNodeTo() {
		return toNode;
	}

	/**
	public double getValue(int position) {
		return values[position];
	}
	*/

	public double[] getValue() {
		return values;
	}

	public String getEdgeNotes() {
		return edgeNotes;
	}

	@Override
	public String toString() {
		return "Edge " + edgeNotes + "[values=" + Arrays.toString(values) + ", fromNode=" + fromNode.getName() + ", toNode="
				+ toNode.getName() + "]";
	}

	public String toStringShort() {
		return fromNode.getName() + ";" + toNode.getName() + ";" + Arrays.toString(values);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(values);
		result = prime * result + Objects.hash(fromNode, toNode);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Edge other = (Edge) obj;
		return Objects.equals(fromNode, other.fromNode) && Objects.equals(toNode, other.toNode)
				&& Arrays.equals(values, other.values) && edgeNotes.equals(other.getEdgeNotes());
	}

	public Edge getCalculatedEdge(ValueOperationCrate valueOperation) {
		double[] values = ArrayFunctionsGeneral.getCopy(this.values);
		values[valueOperation.result] = ValueOperation.getResult(values[valueOperation.operand1],
				valueOperation.operation, values[valueOperation.operand2]);
		return new Edge(this.fromNode, this.toNode, values);
	}

}
