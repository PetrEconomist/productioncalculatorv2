package graph.edge;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import graph.node.Node;

public class EdgesNewTest {

	
	/*
	e1a A -> B
	e2 D ->E
	e1b A -> C
	*/
	
	Edge eBA, eCA, eDC, eEC;
	Node nA = new Node("A");
	Node nB = new Node("B");
	Node nC = new Node("C");
	Node nD = new Node("D");
	Node nE = new Node("E");
	double valBA = 1, valCA = 2, valDC = 6, valEC = 3;		
	

	
	Edge e;	
	Node nodeFrom = new Node("A");
	Node nodeTo = new Node("B");
	int eVal = 5;

	EdgesNew edgesNew = new EdgesNew();
	
	Set<Node> leaves;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		e = new Edge(nodeFrom, nodeTo, eVal);
		
		edgesNew = new EdgesNew();
		
		eBA = new Edge(nB, nA, valBA);
		eCA = new Edge(nC, nA, valCA);
		eDC = new Edge(nD, nC, valDC);
		eEC = new Edge(nE, nC, valEC);

		
		edgesNew.addEdge(eBA);
		edgesNew.addEdge(eCA);
		edgesNew.addEdge(eDC);
		edgesNew.addEdge(eEC);
		
		leaves = new HashSet<Node>();
		leaves.add(nA);

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddEdge() {
		EdgesNew tested = new EdgesNew();
		tested.addEdge(e);
		assertEquals(tested.toString(), "Edges [edges={EdgeNodes [nodeFrom=Node [name=A], nodeTo=Node [name=B]]=[Edge [values=[5.0], fromNode=A, toNode=B]]}]");
	}


	
	@Test
	public void testEqualsObject() {
		assertFalse(edgesNew.equals(null));
		assertFalse(edgesNew.equals(new EdgesNew()));
		assertFalse(edgesNew.equals(new Object()));
		
		EdgesNew edgesNewCopy = new EdgesNew();
		edgesNewCopy.addEdge(eBA);
		edgesNewCopy.addEdge(eCA);
		edgesNewCopy.addEdge(eDC);
		edgesNewCopy.addEdge(eEC);
		assertTrue(edgesNew.equals(edgesNewCopy));
		
		assertTrue(edgesNew.equals(edgesNew));
	}
	

	@Test
	public void testGetRoots() {
		assertEquals(leaves, edgesNew.getLeaves());
	}


	@Test
	public void testGetEdges(){
		EdgesNew en = new EdgesNew();
		Edge eBA = new Edge("B", "A", new double[] {2, 1}, "");
		Edge eCB1 = new Edge("C", "B", new double[] {5, 1}, "");
		Edge eCB2 = new Edge("C", "B", new double[] {7, 1}, "");
		Edge eCB3 = new Edge("C", "B", new double[] {4, 1}, "");
		Edge eCB4 = new Edge("C", "B", new double[] {8, 1}, "");
		en.addEdge(eBA);
		en.addEdge(eCB1);
		en.addEdge(eCB2);
		en.addEdge(eCB3);
		en.addEdge(eCB4);
		
		HashSet<Edge> expected = new HashSet<Edge>();
		expected.add(eBA);
		assertEquals(expected, en.getEdges(new Node("B"), new Node("A")));
		expected.clear();;
		expected.add(eCB1);
		expected.add(eCB2);
		expected.add(eCB3);
		expected.add(eCB4);
		assertEquals(expected, en.getEdges(new Node("C"), new Node("B")));
	}
	
	@Test
	public void testAddExistingEdge(){
		EdgesNew en = new EdgesNew();
		en.addEdge(new Edge("C", "B", new double[] {4.5, 1.3}, "edgeCB"));
		en.addEdge(new Edge("C", "B", new double[] {4.5, 1.3}, "edgeCB"));
		
		HashSet<Edge> expected = new HashSet<Edge>();
		expected.add(new Edge("C", "B", new double[] {9, 2.6}, "edgeCB"));
		assertEquals(expected, en.getEdges(new Node("C"), new Node("B")));

	}
	
	@Test
	public void testSumEdges() {
		EdgesNew en = new EdgesNew();
		en.addEdge(new Edge("C", "B", new double[] {4, 1}, "edgeCB1"));
		en.addEdge(new Edge("C", "B", new double[] {2, 2}, "edgeCB2"));
		en.addEdge(new Edge("C", "B", new double[] {3, 3}, "edgeCB2"));
		en.sumEdges();
		
		HashSet<Edge> expected = new HashSet<Edge>();
		expected.add(new Edge("C", "B", new double[] {9, 6}, ""));
		assertEquals(expected, en.getEdges(new Node("C"), new Node("B")));
	}
	
	@Test
	public void testSumEdges2() {
		EdgesNew en = new EdgesNew();
		en.addEdge(new Edge("C", "B", new double[] {4, 1}, "edgeCB1"));
		en.addEdge(new Edge("D", "B", new double[] {5, 2}, "edgeCB2"));
		en.sumEdges();
		
		HashSet<Edge> expectedCB = new HashSet<Edge>();
		expectedCB.add(new Edge("C", "B", new double[] {4, 1}, ""));
		assertEquals(expectedCB, en.getEdges(new Node("C"), new Node("B")));
		
		HashSet<Edge> expectedDB = new HashSet<Edge>();
		expectedDB.add(new Edge("D", "B", new double[] {5, 2}, ""));
		assertEquals(expectedDB, en.getEdges(new Node("D"), new Node("B")));
	}

	@Test
	public void testSumProductions() {
		//case 1
				//DN1 1xA -> 2xC 
				//DN1 2xB -> 2xC
				//DN2 3xA -> 6xC 
				//DN2 6xB -> 6xC
				//sum of case 1
				//sum 4xA -> 8xC
				//sum 8xB -> 8xC
				//case2: DN from case 1 + DN 5xY -> 2xC
				//sum of case 2
				// total product = 8xC
				//sum 4xA -> 10xC
				//sum 8xB -> 10xC
				//sum 5xY -> 10xC
		
		EdgesNew case1Input = new EdgesNew();
		case1Input.addEdge(new Edge(new Node("A"), new Node("C"), new double[] {1,2}, "DN1"));
		case1Input.addEdge(new Edge(new Node("B"), new Node("C"), new double[] {2,2}, "DN1"));
		case1Input.addEdge(new Edge(new Node("A"), new Node("C"), new double[] {3,6}, "DN2"));
		case1Input.addEdge(new Edge(new Node("B"), new Node("C"), new double[] {6,6}, "DN2"));
		EdgesNew case1Output = new EdgesNew(case1Input);
		case1Output.sumProductions();
		
		EdgesNew case1Expected = new EdgesNew();
		case1Expected.addEdge(new Edge(new Node("A"), new Node("C"), new double[] {4,8}, "summedEdge"));
		case1Expected.addEdge(new Edge(new Node("B"), new Node("C"), new double[] {8,8}, "summedEdge"));
		
		assertEquals(case1Expected, case1Output);
		
		EdgesNew case2Input = new EdgesNew(case1Input);
		case2Input.addEdge(new Edge(new Node("Y"), new Node("C"), new double[] {5,2}, "DN3"));
		EdgesNew case2Output = new EdgesNew(case2Input);
		case2Output.sumProductions();
		
		EdgesNew case2Expected = new EdgesNew();
		case2Expected.addEdge(new Edge(new Node("A"), new Node("C"), new double[] {4,10}, "summedEdge"));
		case2Expected.addEdge(new Edge(new Node("B"), new Node("C"), new double[] {8,10}, "summedEdge"));
		case2Expected.addEdge(new Edge(new Node("Y"), new Node("C"), new double[] {5,10}, "summedEdge"));
		
		assertEquals(case2Expected, case2Output);
	}
	


}
