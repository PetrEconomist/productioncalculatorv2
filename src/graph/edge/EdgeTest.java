package graph.edge;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import graph.node.Node;

public class EdgeTest {

	private Edge testEdge;
	private Node nodeFrom = new Node("A");
	private Node nodeTo = new Node("B");
	private int edgeVal = 5;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		testEdge = new Edge(nodeFrom, nodeTo, edgeVal);
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testEqualsNull() {
		assertFalse(testEdge.equals(null));
	}
	
	@Test
	public void testEqualsMe() {
		assertTrue(testEdge.equals(testEdge));
	}
	
	@Test
	public void testEqualsOther() {
		assertFalse(testEdge.equals(new Object()));
	}
	
	@Test 
	public void testEquals() {
		assertTrue(testEdge.equals(new Edge(nodeFrom, nodeTo, edgeVal)));
		assertFalse(testEdge.equals(new Edge(new Node("C"), nodeTo, edgeVal)));
		assertFalse(testEdge.equals(new Edge(nodeFrom, new Node("C"), edgeVal)));
		assertFalse(testEdge.equals(new Edge(nodeFrom, nodeTo, edgeVal + 1)));
	}


	@Test
	public void testGetNodeFrom() {
		Node nf = testEdge.getNodeFrom();
		assertEquals(nf, nodeFrom);
	}

	@Test
	public void testGetNodeTo() {
		Node nt = testEdge.getNodeTo();
		assertEquals(nt, nodeTo);
	}

	@Test
	public void testToString() {
		String expected = "Edge [values=[5.0], fromNode=A, toNode=B]";
		assertEquals(expected, testEdge.toString());		
	}
	
	


}
