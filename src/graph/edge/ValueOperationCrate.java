package graph.edge;

/**
 * The class is used to store instruction for calculation.
 * @author pope
 *
 */
public class ValueOperationCrate {
	/**
	 * Left-hand side operand ID
	 */
	final int operand1;
	/**
	 * Right-hand side operand ID
	 */
	final int operand2;
	/**
	 * Mathematical operation sigh ID
	 */
	ValueOperationType operation;
	/**
	 * Result ID
	 */
	final int result;

	public ValueOperationCrate(int operand1, ValueOperationType operation, int operand2, int result){
		this.operand1 = operand1;
		this.operand2 = operand2;
		this.operation = operation;
		this.result = result;
	}


	
}
