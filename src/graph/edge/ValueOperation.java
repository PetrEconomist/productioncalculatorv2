package graph.edge;

public class ValueOperation {

	static double getResult(double operand1, ValueOperationType operation, double operand2) {
		
		switch (operation) {
			case ADD:
				return operand1 + operand2;
			case MULTIPLY:
				return operand1 * operand2;
			case SUBTRACT:
				return operand1 - operand2;
			case DIVIDE:
				return operand1 / operand2;
			default:
				throw new Error ("Invalid operation");
		}

	}
}
