package graph.edge;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import graph.node.Node;

public class EdgesTest {
	Edges edges;
	
	Edge eBA, eCA, eDC, eEC;
	Node nA = new Node("A");
	Node nB = new Node("B");
	Node nC = new Node("C");
	Node nD = new Node("D");
	Node nE = new Node("E");
	double valBA = 1, valCA = 2, valDC = 6, valEC = 3;	
	
	Set<Node> roots = new HashSet<Node>();
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		edges = new Edges();
		
		eBA = new Edge(nB, nA, valBA, "e1");
		eCA = new Edge(nC, nA, valCA, "e2");
		eDC = new Edge(nD, nC, valDC, "e3");
		eEC = new Edge(nE, nC, valEC, "e4");
		
		edges.addEdge(eBA);
		edges.addEdge(eCA);
		edges.addEdge(eDC);
		edges.addEdge(eEC);
		
		roots.add(new Node("A"));
	}

	@After
	public void tearDown() throws Exception {
	}



	@Test
	public void testEqualsObject() {
		assertFalse(edges.equals(null));
		assertFalse(edges.equals(new Object()));
		assertTrue(edges.equals(edges));
		Edges other = new Edges();
		other.addEdge(eBA);
		other.addEdge(eCA);
		other.addEdge(eDC);
		other.addEdge(eEC);
		
		
		assertTrue(edges.equals(other));
		other = new Edges();
		other.addEdge(new Edge("A", "B", 1));
		other.addEdge(new Edge("A", "D", 2));
		assertFalse(edges.equals(other));
		other = new Edges();

	}
	
	@Test
	public void testGetRoots() {
		assertEquals(roots, edges.getLeaves());
	}

}
