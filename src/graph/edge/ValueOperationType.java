package graph.edge;

public enum ValueOperationType {
	MULTIPLY, ADD, DIVIDE, SUBTRACT
}
