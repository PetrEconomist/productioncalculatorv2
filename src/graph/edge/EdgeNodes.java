package graph.edge;

import java.util.Objects;

import graph.node.Node;

public class EdgeNodes {
		private Node nodeFrom, nodeTo;
		
		EdgeNodes(Node nodeFrom, Node nodeTo){
			this.nodeFrom = nodeFrom;
			this.nodeTo = nodeTo;
		}
		
		EdgeNodes(Edge e){
			this.nodeFrom = e.getNodeFrom();
			this.nodeTo = e.getNodeTo();
		}
		
		Node getNodeTo() {
			return nodeTo;
		}
		
		Node getNodeFrom() {
			return nodeFrom;
		}

		@Override
		public int hashCode() {
			return Objects.hash(nodeFrom, nodeTo);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			EdgeNodes other = (EdgeNodes) obj;
			return Objects.equals(nodeFrom, other.nodeFrom) && Objects.equals(nodeTo, other.nodeTo);
		}

		@Override
		public String toString() {
			return "EdgeNodes [nodeFrom=" + nodeFrom + ", nodeTo=" + nodeTo + "]";
		}
		
		
	
}
