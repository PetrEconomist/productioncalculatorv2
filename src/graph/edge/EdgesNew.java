package graph.edge;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Consumer;

import arrayFunctions.ArrayFunctionsGeneral;
import graph.node.Node;

public class EdgesNew {

	private static String ATTEMPT_TO_ADD_EXISTING_EDGE_MESSAGE_BEGINNING = "Attempt to add already existing edge ";
	/**
	 * Key: node to <br>
	 * Values: edges direction to the node
	 */
	private HashMap<EdgeNodes, HashSet<Edge>> edges;

	public EdgesNew() {
		edges = new HashMap<EdgeNodes, HashSet<Edge>>();
	}

	public EdgesNew(EdgesNew eo) {
		HashMap<EdgeNodes, HashSet<Edge>> edgesClone = new HashMap<EdgeNodes, HashSet<Edge>>();
		for (EdgeNodes n : eo.edges.keySet()) {
			edgesClone.put(n, eo.edges.get(n));
		}
		this.edges = edgesClone;
	}

	private void addEdgeWithError(Edge edge) throws Error {
		HashSet<Edge> existingEdges = getEdges(new EdgeNodes(edge));
		if (existingEdges.contains(edge)) {
			throw new Error(ATTEMPT_TO_ADD_EXISTING_EDGE_MESSAGE_BEGINNING + edge);
		}
		existingEdges.add(edge);
	}

	/**
	 * Adds edge to the graph. If edge exists in the graph, edges are summed up.
	 * 
	 * @param e edge to be added in the graph
	 */
	public void addEdge(Edge e) {
		;
		addEdge(e, edge -> {
			System.out.println(edge + " summed with edge " + e + " as the edge already existed in the graph");
			Edge sumEdge = new Edge(edge.getNodeFrom(), edge.getNodeTo(),
					ArrayFunctionsGeneral.sumArrays(e.getValue(), edge.getValue()), edge.getEdgeNotes());
			HashSet<Edge> existingEdges = getEdges(new EdgeNodes(edge));
			existingEdges.remove(edge);
			existingEdges.add(sumEdge);
		});
	}

	/**
	 * Adds edge to the graph. If edge exists in the graph, method m is performed.
	 * 
	 * @param e edge to be added in the graph
	 * @param m method to performed if the edge already exists in the graph
	 */
	public void addEdge(Edge e, Consumer<Edge> m) {
		try {
			addEdgeWithError(e);
		} catch (Error err) {
			m.accept(e);
		}
	}

	/**
	 * Gets all edges to node
	 * 
	 * @param node edges directing to the node will be returned
	 * @return edges directing to given node
	 */
	public HashSet<Edge> getEdgesTo(Node nodeTo) {
		HashSet<Edge> out = new HashSet<Edge>();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			if (edgeNodes.getNodeTo().equals(nodeTo)) {
				out.addAll(edges.get(edgeNodes));
			}
		}
		return out;
	}

	private Set<EdgeNodes> getEdgesNodes() {
		return edges.keySet();
	}

	/**
	 * 
	 * Returns all edges directing from node nodeFrom to node nodeTo
	 * 
	 * @param nodeFrom starting node of edges
	 * @param nodeTo   ending node of edges
	 * @return all edges in the graph between given nodes, in given direction
	 */
	public HashSet<Edge> getEdges(Node nodeFrom, Node nodeTo) {
		return getEdges(new EdgeNodes(nodeFrom, nodeTo));
	}

	public HashSet<Edge> getEdges(EdgeNodes edgeNodes) {
		if (!edges.containsKey(edgeNodes)) {
			edges.put(edgeNodes, new HashSet<Edge>());
		}
		return edges.get(edgeNodes);
	}

	public HashSet<Edge> getEdges() {
		HashSet<Edge> out = new HashSet<Edge>();
		Set<EdgeNodes> keySet = edges.keySet();
		for (EdgeNodes edgeNodes : keySet) {
			out.addAll(edges.get(edgeNodes));
		}
		return out;
	}

	/**
	 * Returns all edges
	 * 
	 * @return all edges in the graph
	 */
	/**
	 * public HashSet<Edge> getEdgesFrom(Node nodeFrom) { HashSet<Edge> out = new
	 * HashSet<Edge>(); Set<EdgeNodes> edgesNodes = getEdgesNodes(); for (EdgeNodes
	 * edgeNodes : edgesNodes) { if (edgeNodes.getNodeFrom().equals(nodeFrom)) {
	 * out.addAll(edges.get(edgeNodes)); } } return out; }
	 */

	/**
	 * @Override public int hashCode() { return Objects.hash(edges); }
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EdgesNew other = (EdgesNew) obj;
		return Objects.equals(edges, other.edges);
	}

	@Override
	public String toString() {
		return "Edges [edges=" + edges + "]";
	}

	Set<Node> getLeaves() {
		Set<Node> successors = new HashSet<Node>();
		Set<Node> predecessors = getParents();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			predecessors.add(edgeNodes.getNodeFrom());
			successors.add(edgeNodes.getNodeTo());
		}

		Set<Node> leaves = new HashSet<Node>();
		for (Node parent : successors) {
			if (!predecessors.contains(parent)) {
				leaves.add(parent);
			}
		}
		return leaves;
	}

	Set<Node> getOffspring() {
		HashSet<Node> out = new HashSet<Node>();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			out.add(edgeNodes.getNodeTo());
		}
		return out;
	}

	/**
	 * Gets predecessors and all its successors
	 * 
	 * @return map where predecessor is the key and values are all its successors
	 */
	public HashMap<Node, HashSet<Node>> getSuccessors() {
		HashMap<Node, HashSet<Node>> out = new HashMap<Node, HashSet<Node>>();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			Node nodeFrom = edgeNodes.getNodeFrom();
			Node nodeTo = edgeNodes.getNodeTo();
			HashSet<Node> successors = new HashSet<Node>();
			if (!out.containsKey(nodeFrom)) {
				out.put(nodeFrom, successors);
			}
			out.get(nodeFrom).add(nodeTo);
		}
		return out;
	}

	/**
	 * Gets successors and all its predecessors
	 * 
	 * @return map where successors is the key and values are all its predecessors
	 */
	public HashMap<Node, HashSet<Node>> getPredecessors() {
		HashMap<Node, HashSet<Node>> out = new HashMap<Node, HashSet<Node>>();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			Node nodeFrom = edgeNodes.getNodeFrom();
			Node nodeTo = edgeNodes.getNodeTo();
			HashSet<Node> predecessors = new HashSet<Node>();
			if (!out.containsKey(nodeTo)) {
				out.put(nodeTo, predecessors);
			}
			out.get(nodeTo).add(nodeFrom);
		}
		return out;
	}

	public Set<Node> getParents() {
		HashSet<Node> out = new HashSet<Node>();
		Set<EdgeNodes> edgesNodes = getEdgesNodes();
		for (EdgeNodes edgeNodes : edgesNodes) {
			out.add(edgeNodes.getNodeFrom());
		}
		return out;
	}

	// version of sumEdges before lambda (3/11/22)
//	public void sumEdges() {
//		for (EdgeNodes edgeNodes : edges.keySet()) {
//			HashSet<Edge> edgesSum = new HashSet<Edge>();
//			edgesSum.add(getSumOfEdges(edges.get(edgeNodes)));
//			edges.replace(edgeNodes, edgesSum);
//		}
//	}

	// new version of sumEdges using lambda (from 3/11/22)
	// TODO modify so that inner implementation is not know to the public (e.i.
	// edges.keySet is not publicly known implementation detail
	// TODO teď potřebuji udělat verzi takovou, že si budu moct navolit, že se mají
	// a/ nejdřív sečíst všechny stejné hrany (tj. stejný note) - suma pouze první
	// pozce
	// b/ poté všechny node ve stejném směru suma na pozice 1 i 2
	public void sumEdges() {
		sumEdges(edges.keySet(), edgeNodesSet -> {
			for (EdgeNodes edgeNodes : edgeNodesSet) {
				HashSet<Edge> edgesSum = new HashSet<Edge>();
				edgesSum.add(getSumOfEdges(edges.get(edgeNodes)));
				edges.replace(edgeNodes, edgesSum);
			}
		});

	}
	
	public double getTotalProducedQuantity(Node product) {
		/**
		 * dispatch notes and its produced quantity (definite, therefore not sum)
		 */
		HashMap<String, Double> producedQtByDN = new HashMap<String, Double>();
		HashSet<Edge> dispatchNotes = getEdgesTo(product);
		// loop through productions so that total consumption and production is
		// calculated
		for (Edge dn : dispatchNotes) {
			// add dispatch note's production
			putDNProduction(producedQtByDN, dn);
		}
		
		return producedQtByDN.values().stream().mapToDouble(Double::doubleValue).sum();
	}
	
	private void putDNProduction(HashMap<String, Double> producedQt, Edge dn) {
		String dispatchNoteID = dn.getEdgeNotes();
		if (!producedQt.containsKey(dispatchNoteID)) {
			producedQt.put(dispatchNoteID, dn.getValue()[1]); // TODO literal: produced quantity stored at 2nd position
		} else {
			if (producedQt.get(dispatchNoteID) != dn.getValue()[1]) {
				throw new Error("Produced quantity not definite for dispatch note " + dispatchNoteID);
			}
		}
	}

	public Map<Node, Double> getConsumedQuantityByConsumedID(Node product) {
		/**
		 * consumed ID and its total consumed quantity
		 */
		HashMap<Node, Double> consumedQuantityByConsumedID = new HashMap<Node, Double>();
		/**
		 * all productions of the product
		 */
		HashSet<Edge> dispatchNotes = getEdgesTo(product);
		for (Edge dn : dispatchNotes) {
			// sum consumption of consumable
			incrementConsumptionOfConsumeble(consumedQuantityByConsumedID, dn);
		}
		
		return consumedQuantityByConsumedID;
	}
	
	private void incrementConsumptionOfConsumeble(HashMap<Node, Double> consumedQuantity, Edge dn) {
		Node consumedID = dn.getNodeFrom();
		if (!consumedQuantity.containsKey(consumedID)) {
			consumedQuantity.put(consumedID, dn.getValue()[0]); // TODO literal: produced quantity stored at 2nd
																// position
		} else {
			consumedQuantity.put(consumedID, consumedQuantity.get(consumedID) + dn.getValue()[0]);
		}
	}

	/**
	 * Sums consumptions and productions in edges of the production Graph
	 */
	public void sumProductions() {
		// temporary storage of the summed edges
		EdgesNew edgesWithTotalProductions = new EdgesNew();

		Set<Node> products = getOffspring();
		for (Node product : products) {
			/**
			 * consumed ID and its total consumed quantity
			 */
			Map<Node, Double> consumedQuantityByConsumedID = getConsumedQuantityByConsumedID(product);
			// calculate total production
			double totalProducedQt = getTotalProducedQuantity(product);
			// create summed edges
			consumedQuantityByConsumedID.keySet().stream()
					.forEach(consumedID -> edgesWithTotalProductions.addEdge(new Edge(consumedID, product,
							new double[] { consumedQuantityByConsumedID.get(consumedID), totalProducedQt },
							"summedEdge")));

		}
		this.edges = edgesWithTotalProductions.edges;

		// sum: consumed quantity: sum of edges values; produced quantity set to total
		// production
		// case 1
		// DN1 1xA -> 2xC
		// DN1 2xB -> 2xC
		// DN2 3xA -> 6xC
		// DN2 6xB -> 6xC
		// sum of case 1
		// sum 4xA -> 8xC
		// sum 8xB -> 8xC
		// case2: DN from case 1 + DN 5xY -> 2xC
		// sum of case 2
		// total product = 8xC
		// sum 4xA -> 10xC
		// sum 8xB -> 10xC
		// sum 5xY -> 10xC

		// get produced quantity pro dispatch note for the given product
		// sum: consumed quantity: sum of edges values; produced quantity set to total
		// production
		// case 1
		// DN1 1xA -> 2xC
		// DN1 2xB -> 2xC
		// DN2 3xA -> 6xC
		// DN2 6xB -> 6xC
		// sum of case 1
		// sum 4xA -> 8xC
		// sum 8xB -> 8xC
		// case2: DN from case 1 + DN 5xY -> 2xC
		// sum of case 2
		// total product = 8xC
		// sum 4xA -> 10xC
		// sum 8xB -> 10xC
		// sum 5xY -> 10xC
	}



	private void sumEdges(Set<EdgeNodes> edgeNodesSet, Consumer<Set<EdgeNodes>> c) {
		c.accept(edgeNodesSet);
	}

	private Edge getSumOfEdges(HashSet<Edge> edgesFromNodeAtoNodeB) {
		if (!areSameDirection(edgesFromNodeAtoNodeB)) {
			throw new Error("Edges should be of the same direction");
		}
		Edge sumEdge = null;
		for (Edge e : edgesFromNodeAtoNodeB) {
			if (sumEdge == null) {
				sumEdge = new Edge(e.getNodeFrom(), e.getNodeTo(), new double[1]);
			}
			double[] sum = ArrayFunctionsGeneral.sumArrays(sumEdge.getValue(), e.getValue());
			sumEdge = new Edge(e.getNodeFrom(), e.getNodeTo(), sum);

		}
		return sumEdge;
	}

	/**
	 * public void averageEdges() { for (EdgeNodes edgeNodes : edges.keySet()) {
	 * HashSet<Edge> edgesSum = new HashSet<Edge>();
	 * edgesSum.add(getAverageOfEdges(edges.get(edgeNodes)));
	 * edges.replace(edgeNodes, edgesSum); } }
	 */

	/**
	 * private Edge getAverageOfEdges(HashSet<Edge> edgesFromNodeAtoNodeB) { Edge
	 * sum = getSumOfEdges(edgesFromNodeAtoNodeB); double[] averages =
	 * ArrayFunctionsGeneral.divideArray(sum.getValue(),
	 * edgesFromNodeAtoNodeB.size()); return new Edge(sum.getNodeFrom(),
	 * sum.getNodeTo(), averages); }
	 */

	private boolean areSameDirection(HashSet<Edge> edges) {
		HashSet<Node> fromNodes = new HashSet<Node>();
		HashSet<Node> toNodes = new HashSet<Node>();

		for (Edge e : edges) {
			fromNodes.add(e.getNodeFrom());
			toNodes.add(e.getNodeTo());

			if ((fromNodes.size() != 1) || (toNodes.size() != 1)) {
				return false;
			}
		}
		return true;
	}

	public void calculateEdgesValue(ValueOperationCrate valueOperation) {
		for (EdgeNodes edgeNodes : edges.keySet()) {
			HashSet<Edge> calculatedEdges = getCalculatedEdges(edges.get(edgeNodes), valueOperation);
			edges.replace(edgeNodes, calculatedEdges);
		}
	}

	private HashSet<Edge> getCalculatedEdges(HashSet<Edge> edgesForCalculation, ValueOperationCrate valueOperation) {
		HashSet<Edge> calculatedEdges = new HashSet<Edge>();
		for (Edge e : edgesForCalculation) {
			calculatedEdges.add(e.getCalculatedEdge(valueOperation));
		}
		return calculatedEdges;
	}

	public LinkedList<String> getExportBatch() {
		LinkedList<String> out = new LinkedList<String>();
		for (Edge e : getEdges()) {
			out.add(e.toStringShort());
		}
		return out;
	}

}
