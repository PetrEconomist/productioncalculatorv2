package graph;

import graph.edge.Edge;

public class InputProcessor {
	private static final String INPUT_SEPARATOR = ";";
	private static final String VALUES_DELIMINATOR_LEFT = "[";
	private static final String VALUES_DELIMINATOR_RIGHT = "]";
	private static final int POSITION_NOTES = 0;
	private static final int POSITION_NODE_FROM = 1;
	private static final int POSITION_NODE_TO = 2;

	private Edge inputEdge;
	private String inputToBeProcessed;

	InputProcessor(String input) {
		this.inputToBeProcessed = input;
		setEdgeFromInput();
	}

	private void setEdgeFromInput() {
		double[] values = getValuesFromInput();
		String[] notesAndNodes = getNotesAndNodesFromInput();
		this.inputEdge = new Edge(notesAndNodes[POSITION_NODE_FROM], notesAndNodes[POSITION_NODE_TO], values,
				notesAndNodes[POSITION_NOTES]);
	}

	private String[] getNotesAndNodesFromInput() {
		String[] notesAndNodes = new String[POSITION_NODE_TO + 1];
		notesAndNodes[POSITION_NODE_TO] = inputToBeProcessed
				.substring(inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR) + 1);
		inputToBeProcessed = inputToBeProcessed.substring(0, inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR));
		notesAndNodes[POSITION_NODE_FROM] = inputToBeProcessed
				.substring(inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR) + 1);
		try {
			notesAndNodes[POSITION_NOTES] = inputToBeProcessed.substring(0,
					inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR));
		} catch (Exception e) {
			notesAndNodes[POSITION_NOTES] = "";
		}

		return notesAndNodes;
	}

	private double[] getValuesFromInput() {
		String valuesInput = getValuesInput();
		String[] valuesStr = valuesInput.split(INPUT_SEPARATOR);
		double[] values = new double[valuesStr.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = Double.parseDouble(valuesStr[i]);
		}
		return values;
	}

	private String getValuesInput() {
		String valuesInput;
		if (inputEndsWithValuesDeliminator()) {
			valuesInput = inputToBeProcessed.substring(inputToBeProcessed.lastIndexOf(VALUES_DELIMINATOR_LEFT) + 1,
					inputToBeProcessed.length() - 1);
			inputToBeProcessed = inputToBeProcessed.substring(0,
					inputToBeProcessed.lastIndexOf(VALUES_DELIMINATOR_LEFT) - 1); // separator is prior to bracelet
		} else {
			valuesInput = inputToBeProcessed.substring(inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR) + 1,
					inputToBeProcessed.length());
			inputToBeProcessed = inputToBeProcessed.substring(0, inputToBeProcessed.lastIndexOf(INPUT_SEPARATOR));
		}

		return valuesInput;
	}

	private boolean inputEndsWithValuesDeliminator() {
		String lastSymbol = inputToBeProcessed.substring(inputToBeProcessed.length() - 1);
		if (lastSymbol.equals(VALUES_DELIMINATOR_RIGHT)) {
			return true;
		} else {
			return false;
		}
	}

	Edge getInputEdge() {
		return inputEdge;
	}

}
