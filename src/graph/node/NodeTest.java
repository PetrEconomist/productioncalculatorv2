package graph.node;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class NodeTest{
	String nodeName = "A";
	Node testNode = new Node(nodeName);
	
	@Test
	public void getNameTest() {	  
	      assertEquals(nodeName,testNode.getName());
	   }

	@Test
	public void toStringTest() {
		String nodeToString = "Node [name=" + nodeName + "]";
		assertEquals(nodeToString, testNode.toString());
	}
	
	@Test
	public void hashCodeTest() {
		int hashExpected = 96;
		assertEquals(hashExpected, testNode.hashCode());
	}
	
	@Test
	public void equalsTestNull() {
		assertFalse(testNode.equals(null));
	}
	
	@Test 
	public void equalsTestMe() {
		assertTrue(testNode.equals(testNode));
	}
	
	@Test
	public void equalsTestOther() {
		assertFalse(testNode.equals(new Node("B")));
	}
	
	@Test
	public void equalsTestOtherObject() {
		Object o = new Object();
		assertFalse(testNode.equals(o));
	}
	


}
