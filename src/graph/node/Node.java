package graph.node;

import java.util.Objects;

public class Node {
	private String name;


	public Node(String name) {
		this.name = name;

	}
	
	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return "Node [name=" + name + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Node other = (Node) obj;
		return Objects.equals(name, other.name);
	}


}
