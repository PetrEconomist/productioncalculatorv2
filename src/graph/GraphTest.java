package graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import graph.edge.Edge;
import graph.node.Node;


public class GraphTest {

	private Graph graph;
	LinkedList<String> testInput;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		graph = new Graph();
		
		testInput = new LinkedList<String>();
		testInput.add("B;A;1");
		testInput.add("C;A;2");
		testInput.add("D;C;3");
		testInput.add("E;C;1.5");	
		
		graph.addEdgesFromInput(testInput);
	}
	
	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testGetDilutedGraph() {
		Graph expected = new Graph();
		expected.addEdge("D;A;6");
		expected.addEdge("B;A;1");
		expected.addEdge("E;A;3");
		Graph diluted = graph.getDilutedGraph();
		assertEquals(expected, diluted);
	}

	@Test
	public void testAddEdgesFromInput() {

		String expectedToString = "Graph [edges=Edges ["
				+ "edgesToNode=Edges [edges={"
				+ "EdgeNodes [nodeFrom=Node [name=B], nodeTo=Node [name=A]]=[Edge [values=[1.0], fromNode=B, toNode=A]], "
				+ "EdgeNodes [nodeFrom=Node [name=D], nodeTo=Node [name=C]]=[Edge [values=[3.0], fromNode=D, toNode=C]], "
				+ "EdgeNodes [nodeFrom=Node [name=C], nodeTo=Node [name=A]]=[Edge [values=[2.0], fromNode=C, toNode=A]], "
				+ "EdgeNodes [nodeFrom=Node [name=E], nodeTo=Node [name=C]]=[Edge [values=[1.5], fromNode=E, toNode=C]]"
				+ "}]]]";
		
		String toStringResult = graph.toString();
		assertEquals(expectedToString, toStringResult);
	}
	
	@Test
	public void testEquals() {
		assertFalse(graph.equals(null));
		assertFalse(graph.equals(new Object()));
		assertTrue(graph.equals(graph));
		Graph other = new Graph();
		other.addEdgesFromInput(testInput);
		assertTrue(graph.equals(other));
	}
	
	@Test
	public void testInputNull() {
		Graph g = new Graph();
		g.addEdgesFromInput(null);
		assertEquals(new Graph(), g);
	}
	
	@Test
	public void testConsolidatedGraph() {
		Graph g = new Graph();
		g.addEdge(new Edge("B", "A", 1, "edge1"));
		g.addEdge(new Edge("B", "A", 1, "edge2"));
		g.consolidate();
		
		Graph gExpected = new Graph();
		Edge eExpected = new Edge("B", "A", 2);
		gExpected.addEdge(eExpected);
		
		assertEquals(g, gExpected);
	}
	
	@Test
	public void testGetPredecessors() {
		HashMap<Node, HashSet<Node>> expected = new HashMap<Node, HashSet<Node>>();
		HashSet<Node> succA = new HashSet<Node>();
		succA.add(new Node("B"));
		succA.add(new Node("C"));
		expected.put(new Node("A"), succA);
		HashSet<Node> succC = new HashSet<Node>();
		succC.add(new Node("D"));
		succC.add(new Node("E"));
		expected.put(new Node("C"), succC);
		
		assertEquals(expected, graph.getPredecessors());
		
	}
	
	@Test
	public void testGetSuccessors() {
		HashMap<Node, HashSet<Node>> expected = new HashMap<Node, HashSet<Node>>();
		HashSet<Node> succB = new HashSet<Node>();
		succB.add(new Node("A"));
		expected.put(new Node("B"), succB);
		HashSet<Node> succC = new HashSet<Node>();
		succC.add(new Node("A"));
		expected.put(new Node("C"), succC);
		HashSet<Node> succD = new HashSet<Node>();
		succD.add(new Node("C"));
		expected.put(new Node("D"), succD);
		HashSet<Node> succE = new HashSet<Node>();
		succE.add(new Node("C"));
		expected.put(new Node("E"), succE);
		
		assertEquals(expected, graph.getSuccessors());
	}
	
	@Test
	public void testFileExport() throws Throwable {

		Graph graph = new Graph();

		LinkedList<String> testInput= new LinkedList<String>();
		testInput.add("C;A;[2.0]");
		testInput.add("B;A;[1.0]");
		testInput.add("E;C;[1.5]");
		testInput.add("D;C;[3.0]");

		graph.addEdgesFromInput(testInput);
		LinkedList<String> export = graph.getExportBatch();
		
		LinkedList<String> testExpected = new LinkedList<String>();
		testExpected.addAll(testInput);

		assertEquals(testExpected, export);
	}
	
	@Test
	public void testIsAcyclic(){

		assertTrue(graph.isAcyclic());

		Graph cyclicGraph = new Graph();
		
		LinkedList<String> testInputCyclic = new LinkedList<String>();
		testInputCyclic.add("B;A;1");
		testInputCyclic.add("D;B;3");
		testInputCyclic.add("B;C;1.5");	
		testInputCyclic.add("C;D;1.5");	
		
		cyclicGraph.addEdgesFromInput(testInputCyclic);
		
		assertFalse(cyclicGraph.isAcyclic());
		
	}
	
	
	
}
