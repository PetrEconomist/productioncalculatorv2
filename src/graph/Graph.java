package graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import arrayFunctions.ArrayFunctionsGeneral;
import graph.edge.Edge;
import graph.edge.Edges;
import graph.edge.ValueOperationCrate;
import graph.edge.ValueOperationType;
import graph.node.Node;

/*
 * Creates graph from verticles and nodes given.
 */
public class Graph {

	protected Edges edges;

	public Graph() {
		edges = new Edges();
	}

	public Graph(Graph g) {
		this.edges = new Edges(g.edges);
	}

	public void addEdgesFromInput(LinkedList<String> input) {
		if (input == null)
			return;
		for (String in : input) {
			addEdge(in);
		}
	}

	public void addEdge(Edge e) {
		edges.addEdge(e);
	}

	/**
	 * 
	 * @param input (optional) record ID, description etc. ; Node from (name) ; Node
	 *              to (name) ; [VALUE;VALUE;VALUE...]
	 */
	public void addEdge(String input) {
		addEdge(new InputProcessor(input).getInputEdge());
	}

	public Set<Node> getLeaves() {
		return edges.getLeaves();
	}

	@Override
	public String toString() {
		return "Graph [edges=" + edges + "]";
	}

	/**
	 * @Override public int hashCode() { return Objects.hash(edges); }
	 */

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Graph other = (Graph) obj;
		return Objects.equals(edges, other.edges);
	}

	/**
	 * Gets diluted graph. <br>
	 * By diluted is meant that only leaves and roots remain, any nodes between them
	 * are removed and values recalculated.
	 * 
	 * @return Graph with recalculated values, only roots and leaves remain in the
	 *         graph.
	 */
	public Graph getDilutedGraph() {
		if (!this.isAcyclic()) {
			throw new IndexOutOfBoundsException("To dilute the graph, it needs to be acyclic");
		}
		Graph dilutedGraph = new Graph();
		Set<Node> leaves = getLeaves();
		for (Node leaf : leaves) {
			List<Edge> dilutedEdges = diluteLeaves(leaf);
			for (Edge e : dilutedEdges) {
				dilutedGraph.addEdge(e);
			}
		}
		dilutedGraph.consolidate();
		return dilutedGraph;
	}

	protected List<Edge> diluteLeaves(Node leaf) {
		return diluteEdges(leaf, leaf, null);
	}

	private List<Edge> diluteEdges(Node exploredNode, Node leaf, double[] coefficient) {
		// list where diluted edges are stored
		List<Edge> edgesRootsLeaf = new LinkedList<Edge>();
		// get edges directing to node I currently explore
		HashSet<Edge> edgesToExploredNode = getEdgesToNode(exploredNode);
		// explore further all children of explored node
		if (!edgesToExploredNode.isEmpty()) {
			for (Edge edgeToExploredNode : edgesToExploredNode) {
				// coefficient is null at the beginning (starting at root)
				if (coefficient == null) {
					edgesRootsLeaf
							.addAll(diluteEdges(edgeToExploredNode.getNodeFrom(), leaf, edgeToExploredNode.getValue()));
				} else {
					edgesRootsLeaf.addAll(diluteEdges(edgeToExploredNode.getNodeFrom(), leaf,
							ArrayFunctionsGeneral.multiplyArrays(edgeToExploredNode.getValue(), coefficient)));
				}
			}
			// exploredNode has no children, which means exploredParent is leaf
			// store edge between root and leaf
		} else {
			edgesRootsLeaf.add(new Edge(exploredNode, leaf, coefficient));
		}
		// recursion ended, return calculated values
		return edgesRootsLeaf;
	}

	private HashSet<Edge> getEdgesToNode(Node nodeTo) {
		return edges.getEdgesToNode(nodeTo);
	}

	public HashSet<Node> getSuccessors(Node n) {
		return edges.getSuccessors(n);
	}

	public HashMap<Node, HashSet<Node>> getSuccessors() {
		return edges.getSuccessors();
	}

	public HashSet<Node> getPredecessors(Node n) {
		return edges.getPredecessors(n);
	}

	public HashMap<Node, HashSet<Node>> getPredecessors() {
		return edges.getPredecessors();
	}

	public HashSet<Edge> getEdges() {
		return edges.getEdges();
	}

	public void consolidate() {
		sumEdges();
	}

	protected void sumEdges() {
		edges.sumEdges();
	}

	public HashSet<Edge> getEdges(Node fromNode, Node toNode) {
		return edges.getEdges(fromNode, toNode);
	}

	public void calculateEdgesValue(int valueOperand1Position, ValueOperationType operation, int valueOperand2Position,
			int valueResultPosition) {
		calculateEdgesValue(
				new ValueOperationCrate(valueOperand1Position, operation, valueOperand2Position, valueResultPosition));
	}

	public void calculateEdgesValue(ValueOperationCrate valueOperation) {
		edges.calculateEdgesValue(valueOperation);
	}

	public HashSet<Node> getNodes() {
		HashSet<Edge> edgesAll = getEdges();
		HashSet<Node> nodes = new HashSet<Node>();
		for (Edge e : edgesAll) {
			nodes.add(e.getNodeFrom());
			nodes.add(e.getNodeTo());
		}
		return nodes;
	}

	public LinkedList<String> getExportBatch() {
		return edges.getExportBatch();

	}

	public boolean isAcyclic() {
		Set<Node> leaves = this.getLeaves();
		for (Node leaf : leaves) {
			if (!hasAcyclicPredecessors(leaf, List.of(leaf))) {
				return false;
			}
		}
		return true;
	}

	private boolean hasAcyclicPredecessors(Node offspring, List<Node> visited) {
		Set<Node> predecessors = this.getPredecessors(offspring);
		if (predecessors != null) {
			// DFS
			for (Node predecessor : predecessors) {
				if (visited.contains(predecessor)) {
					System.out.println("Cyclus found in the graph: " + visited + " " + predecessor + " \n");
					return false;
				} else {
					List<Node> nextPath = new LinkedList<Node>(visited);
					nextPath.add(predecessor);
					return hasAcyclicPredecessors(predecessor, nextPath);
				}
			}
		}
		// no successors
		return true;
	}

}
