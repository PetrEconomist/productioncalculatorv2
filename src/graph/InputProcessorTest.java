package graph;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import graph.edge.Edge;
import graph.node.Node;

public class InputProcessorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testInputProcessor() {
		assertEquals(new InputProcessor("A;B;1").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, ""));
		assertEquals(new InputProcessor("note;A;B;1").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, "note"));
		assertEquals(new InputProcessor(";A;B;1").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, ""));
		assertEquals(new InputProcessor("note1;note2;A;B;1").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, "note1;note2"));
		assertEquals(new InputProcessor("note;A;B;[1]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, "note"));
		assertEquals(new InputProcessor(";A;B;[1]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, ""));
		assertEquals(new InputProcessor("note1;note2;A;B;[1]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0}, "note1;note2"));
		assertEquals(new InputProcessor("note;A;B;[1;2]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0, 2.0}, "note"));
		assertEquals(new InputProcessor(";A;B;[1;2]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0, 2.0}, ""));
		assertEquals(new InputProcessor("note1;note2;A;B;[1;2]").getInputEdge(), new Edge(new Node("A"), new Node("B"), new double[] {1.0, 2.0}, "note1;note2"));
	}



}
