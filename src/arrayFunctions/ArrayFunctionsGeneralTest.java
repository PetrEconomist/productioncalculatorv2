package arrayFunctions;

import static org.junit.Assert.*;

import org.junit.Test;

public class ArrayFunctionsGeneralTest extends ArrayFunctionsGeneral {
	String[] testArr;

	@Test
	public void testPrintlnArrayEmptyNull() {
		String expected = "";
		assertEquals(expected, getArrayToText(testArr));
	}

	@Test
	public void testPrintlnArrayEmpty1() {
		String expected = "0: A\n";
		testArr = new String[1];
		testArr[0] = "A";
		assertEquals(expected, getArrayToText(testArr));
	}

	@Test
	public void testPrintlnArrayEmpty3() {
		String expected = "0: A\n1: B\n2: C\n";
		testArr = new String[3];
		testArr[0] = "A";
		testArr[1] = "B";
		testArr[2] = "C";
		assertEquals(expected, getArrayToText(testArr));
	}

	private String getArrayToText(String[] arr) {
		return printlnArray(testArr);
	}

	@Test
	public void testMultipleArray() {
		double[] testArr = { -1, 0, 1.5, -0.6 };

		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArray(testArr, 1),
				(new double[] { -1, 0, 1.5, -0.6 })));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArray(testArr, -1),
				(new double[] { 1.0, -0.0, -1.5, 0.6 })));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArray(testArr, 2),
				(new double[] { -2.0, 0.0, 3.0, -1.2 })));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArray(testArr, 0),
				(new double[] { -0.0, 0.0, 0.0, -0.0 })));
	}

	@Test
	public void testMultipleArrays() {
		double[] testArr = { -1, 0, 1.5, -0.6 };

		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArrays(testArr, new double[] { 1, 1, 1, 1 }),
				(new double[] { -1, 0, 1.5, -0.6 })));
		assertTrue(
				java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArrays(testArr, new double[] { -1, -1, -1, -1 }),
						(new double[] { 1.0, -0.0, -1.5, 0.6 })));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArrays(testArr, new double[] { 2, 2, 2, 2 }),
				(new double[] { -2.0, 0.0, 3.0, -1.2 })));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.multiplyArrays(testArr, new double[] { 0, 0, 0, 0 }),
				(new double[] { -0.0, 0.0, 0.0, -0.0 })));
	}

	@Test
	public void testSumArrays() {
		double[] arr1 = new double[] { 1, 2, 3, 4 };
		double[] arr2 = new double[] { 2, 3, 4, 5 };
		double[] arr3 = new double[] { -1, -2, -3, -4 };
		double[] arr4 = new double[] { 1, 1 };

		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.sumArrays(arr1, arr2), new double[] { 3, 5, 7, 9 }));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.sumArrays(arr1, arr3), new double[] { 0, 0, 0, 0 }));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.sumArrays(arr1, arr4), new double[] { 2, 3, 3, 4 }));
		assertTrue(java.util.Arrays.equals(ArrayFunctionsGeneral.sumArrays(arr4, arr1), new double[] { 2, 3, 3, 4 }));
	}
}
