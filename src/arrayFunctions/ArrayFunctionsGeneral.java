package arrayFunctions;

public class ArrayFunctionsGeneral {

	public static String printlnArray(String[] arr) {
		String out = "";

		if (arr == null) {
			return out;
		}

		for (int i = 0; i < arr.length; i++) {
			out = out + i + ": " + arr[i] + "\n";
		}
		System.out.println(out);

		return out;
	}

	/**
	public static String arrayToString(double[] arr, String separator) {
		String out = "" + arr[0];
		for (int i = 1; i < arr.length; i++) {
			out = out + separator + arr[i];
		}
		return out;
	}
	*/

	public static double[] multiplyArray(double[] arr, double multiplier) {
		double[] multipliedArr = getCopy(arr);
		for (int i = 0; i < multipliedArr.length; i++) {
			multipliedArr[i] *= multiplier;
		}
		return multipliedArr;
	}

	/**
	public static double[] divideArray(double[] arr, double divisor) {
		return multiplyArray(arr, 1 / divisor);
	}
	*/

	public static double[] sumArrays(double[] arr1, double[] arr2) {
		if (arr1.length < arr2.length) {
			return sumArrays(arr2, arr1);
		}
		double[] sumArr = getCopy(arr1);
		for (int i = 0; i < arr2.length; i++) {
			sumArr[i] += arr2[i];
		}
		return sumArr;
	}

	public static double[] multiplyArrays(double[] arr1, double[] arr2) {
		if (arr1.length != arr2.length) {
			throw new Error("Arrays must be of the same size");
		}
		double[] multipliedArr = getCopy(arr1);
		for (int i = 0; i < multipliedArr.length; i++) {
			multipliedArr[i] *= arr2[i];
		}
		return multipliedArr;
	}

	public static double[] getCopy(double[] arr) {
		double[] copy = new double[arr.length];
		for (int i = 0; i < copy.length; i++) {
			copy[i] = arr[i];
		}
		return copy;
	}
	
	/**
	public static int[] getIntArray(int length, int initialValue) {
		int[] out = new int[length];
		for(int i = 0; i < length; i++) {
			out[i] = initialValue;
		}
		return out;
	}
	*/
	
}
