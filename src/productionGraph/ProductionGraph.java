package productionGraph;

import graph.Graph;
import graph.edge.ValueOperationType;

/**
 * Graph of production.
 * 
 * @author pope
 *
 */
public class ProductionGraph extends Graph {
	private static final int CONSUMED_QUANTITY_ARRAY_POSITION = 0;
	private static final int PRODUCED_QUANTITY_ARRAY_POSITION = 1;
	
	public ProductionGraph() {
		super();
	}

	public ProductionGraph(ProductionGraph productionGraph) {
		super(productionGraph);
	}

	public ProductionGraph(Graph graph) {
		super(graph);
	}

	/**
	 * Gets diluted graph.<br>
	 * By diluted is meant that only leaves and roots remain, any nodes between them
	 * are removed and values recalculated.<br>
	 * Recalculation: If there are more paths between nodes, the mean value of paths
	 * is taken.<br>
	 * Removal of points: Values of paths are multiplied. It means for path C (2x) >
	 * B (3x) > A result is C (2*3x) > A
	 * 
	 * @return Graph with recalculated values, only roots and leaves remain in the
	 *         graph.
	 */

	@Override
	public ProductionGraph getDilutedGraph() {
		// TODO must create parent object, otherwise stack overflow error (due to cyclic
		// calling of overridden method) - solve, rewrite
		Graph diluted = new Graph(this);
		diluted = diluted.getDilutedGraph();

		return new ProductionGraph(diluted);
	}

	@Override
	public String toString() {
		return "ProductionGraph [edges=" + edges + "]";
	}

	public ProductionGraph getConsumptionPerPiece() {
		// sum same production schema (consID/prodID)
		Graph perPiece = new Graph(this);
		perPiece.consolidate();
		// calculate per piece consumption
		// consQt: position 0, prodQt: position 1, calculate consQt/prodQt and store result in position 0
		perPiece.calculateEdgesValue(CONSUMED_QUANTITY_ARRAY_POSITION, ValueOperationType.DIVIDE,
				PRODUCED_QUANTITY_ARRAY_POSITION, CONSUMED_QUANTITY_ARRAY_POSITION);
		// prodQt: set to 1
		perPiece.calculateEdgesValue(PRODUCED_QUANTITY_ARRAY_POSITION, ValueOperationType.DIVIDE,
				PRODUCED_QUANTITY_ARRAY_POSITION, PRODUCED_QUANTITY_ARRAY_POSITION);
		return new ProductionGraph(perPiece);
	}

	public ProductionGraph getDilutedConsumptionPerPiece() {
		ProductionGraph perPiece = this.getConsumptionPerPiece();
		ProductionGraph perPieceDiluted = perPiece.getDilutedGraph();
		//calculate per piece (through dilution could not be anymore per piece
		perPieceDiluted = perPieceDiluted.getConsumptionPerPiece();
		//only consQt should be summed, set prodQt to 1
		perPieceDiluted.calculateEdgesValue(PRODUCED_QUANTITY_ARRAY_POSITION, ValueOperationType.DIVIDE,
				PRODUCED_QUANTITY_ARRAY_POSITION, PRODUCED_QUANTITY_ARRAY_POSITION);
		return perPieceDiluted;
	}

}
